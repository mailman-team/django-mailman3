# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-12 21:00+0000\n"
"PO-Revision-Date: 2023-10-30 19:16+0000\n"
"Last-Translator: Thijs Kinkhorst <thijs@kinkhorst.com>\n"
"Language-Team: Dutch <https://hosted.weblate.org/projects/gnu-mailman/django-"
"mailman3/nl/>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.2-dev\n"

#: forms.py:32
msgid "Username"
msgstr "Gebruikersnaam"

#: forms.py:33
msgid "First name"
msgstr "Voornaam"

#: forms.py:34
msgid "Last name"
msgstr "Achternaam"

#: forms.py:36
msgid "Time zone"
msgstr "Tijdzone"

#: forms.py:43
msgid "A user with that username already exists."
msgstr "Er bestaat al een gebruiker met deze gebruikersnaam."

#: templates/account/email.html:6
#: templates/django_mailman3/profile/base.html:18
msgid "Account"
msgstr "Account"

#: templates/account/email.html:11
msgid "The following e-mail addresses are associated with your account:"
msgstr "Aan uw account zijn de volgende e-mailadressen gekoppeld:"

#: templates/account/email.html:25
msgid "Verified"
msgstr "Geverifieerd"

#: templates/account/email.html:27
msgid "Unverified"
msgstr "Niet geverifieerd"

#: templates/account/email.html:29
msgid "Primary"
msgstr "Primair"

#: templates/account/email.html:35
msgid "Make Primary"
msgstr "Primair maken"

#: templates/account/email.html:36
msgid "Re-send Verification"
msgstr "Verificatie opnieuw versturen"

#: templates/account/email.html:37 templates/socialaccount/connections.html:34
msgid "Remove"
msgstr "Verwijderen"

#: templates/account/email.html:44
msgid "Warning:"
msgstr "Waarschuwing:"

#: templates/account/email.html:44
msgid ""
"You currently do not have any e-mail address set up. You should really add "
"an e-mail address so you can receive notifications, reset your password, etc."
msgstr ""
"U hebt momenteel geen e-mailadres ingesteld. U dient een e-mailadres toe te "
"voegen voor het ontvangen van berichten, het wijzigen van uw wachtwoord, enz."

#: templates/account/email.html:49
msgid "Add E-mail Address"
msgstr "Voeg een e-mailadres toe"

#: templates/account/email.html:55
msgid "Add E-mail"
msgstr "Voeg een e-mail toe"

#: templates/account/email.html:66
msgid "Do you really want to remove the selected e-mail address?"
msgstr "Wilt u het geselecteerde e-mailadres echt verwijderen?"

#: templates/account/email_confirm.html:6
#: templates/account/email_confirm.html:10
msgid "Confirm E-mail Address"
msgstr "E-mailadres bevestigen"

#: templates/account/email_confirm.html:16
#, python-format
msgid ""
"Please confirm that <a href=\"mailto:%(email)s\">%(email)s</a> is an e-mail "
"address for user %(user_display)s."
msgstr ""
"Bevestig dat <a href=\"mailto:%(email)s\">%(email)s</a> een e-mailadres is "
"voor gebruiker %(user_display)s."

#: templates/account/email_confirm.html:20
msgid "Confirm"
msgstr "Bevestig"

#: templates/account/email_confirm.html:27
#, python-format
msgid ""
"This e-mail confirmation link expired or is invalid. Please <a href="
"\"%(email_url)s\">issue a new e-mail confirmation request</a>."
msgstr ""
"Deze e-mailbevestigingslink is verlopen of ongeldig. Dien <a href="
"\"%(email_url)s\"> een verzoek in voor een nieuwe e-mailbevestigingslink </"
"a>."

#: templates/account/login.html:7 templates/account/login.html:11
#: templates/account/login.html:59 templates/socialaccount/login.html:5
msgid "Sign In"
msgstr "Log in"

#: templates/account/login.html:18
#, python-format
msgid ""
"Please sign in with one\n"
"of your existing third party accounts. Or, <a href=\"%(signup_url)s\">sign "
"up</a>\n"
"for a %(site_name)s account and sign in below:"
msgstr ""
"Log in met een\n"
" van uw bestaande derde-partijen-accounts. Of <a href=\"%(signup_url)s\"> "
"log in </a>\n"
" voor een %(site_name)s account en log hieronder in:"

#: templates/account/login.html:22
#, python-format
msgid ""
"If you have a %(site_name)s\n"
"account that you haven't yet linked to any third party account, you must "
"log\n"
"in with your account's email address and password and once logged in, you "
"can\n"
"link your third party accounts via the Account Connections tab on your user\n"
"profile page."
msgstr ""

#: templates/account/login.html:28 templates/account/signup.html:17
#, python-format
msgid ""
"If you do not have a\n"
"%(site_name)s account but you have one of these third party accounts, you "
"can\n"
"create a %(site_name)s account and sign-in in one step by clicking the "
"third\n"
"party account."
msgstr ""

#: templates/account/login.html:41 templates/account/signup.html:30
#: templates/django_mailman3/profile/profile.html:72
msgid "or"
msgstr "of"

#: templates/account/login.html:48
#, python-format
msgid ""
"If you have not created an account yet, then please\n"
"<a href=\"%(signup_url)s\">sign up</a> first."
msgstr ""
"Als u nog geen account heeft aangemaakt\n"
"log dan graag eerst <a href=\"%(signup_url)s\"> in </a>."

#: templates/account/login.html:61
msgid "Forgot Password?"
msgstr "Wachtwoord vergeten?"

#: templates/account/logout.html:5 templates/account/logout.html:8
#: templates/account/logout.html:17
msgid "Sign Out"
msgstr "Log uit"

#: templates/account/logout.html:10
msgid "Are you sure you want to sign out?"
msgstr "Weet u zeker dat u wilt uitloggen?"

#: templates/account/password_change.html:12
#: templates/account/password_reset_from_key.html:6
#: templates/account/password_reset_from_key.html:9
#: templates/django_mailman3/profile/base.html:21
msgid "Change Password"
msgstr "Wijzig uw wachtwoord"

#: templates/account/password_reset.html:7
#: templates/account/password_reset.html:11
msgid "Password Reset"
msgstr "Wachtwoord wijzigen"

#: templates/account/password_reset.html:16
msgid ""
"Forgotten your password? Enter your e-mail address below, and we'll send you "
"an e-mail allowing you to reset it."
msgstr ""
"Wachtwoord vergeten? Vul hieronder uw e-mailadres in, dan sturen wij u een e-"
"mail om het weer in te stellen."

#: templates/account/password_reset.html:22
msgid "Reset My Password"
msgstr "Wijzig mijn wachtwoord"

#: templates/account/password_reset.html:27
msgid "Please contact us if you have any trouble resetting your password."
msgstr "Neem contact met ons op als herstellen van uw wachtwoord niet lukt."

#: templates/account/password_reset_from_key.html:9
msgid "Bad Token"
msgstr "Verkeerde cijfercombinatie"

#: templates/account/password_reset_from_key.html:13
#, python-format
msgid ""
"The password reset link was invalid, possibly because it has already been "
"used.  Please request a <a href=\"%(passwd_reset_url)s\">new password reset</"
"a>."
msgstr ""
"De link voor wachtwoordherstel was ongeldig, mogelijk omdat hij al is "
"gebruikt. Dien een <a href=\"%(passwd_reset_url)s\"> verzoek in voor een "
"nieuwe link </a>."

#: templates/account/password_reset_from_key.html:20
msgid "change password"
msgstr "wachtwoord wijzigen"

#: templates/account/password_reset_from_key.html:25
msgid "Your password is now changed."
msgstr "Uw wachtwoord is gewijzigd."

#: templates/account/password_set.html:12
msgid "Set Password"
msgstr "Wachtwoord instellen"

#: templates/account/signup.html:7 templates/socialaccount/signup.html:6
msgid "Signup"
msgstr "Log in"

#: templates/account/signup.html:10 templates/account/signup.html:42
#: templates/socialaccount/signup.html:9 templates/socialaccount/signup.html:21
msgid "Sign Up"
msgstr "Aanmelden"

#: templates/account/signup.html:12
#, python-format
msgid ""
"Already have an account? Then please <a href=\"%(login_url)s\">sign in</a>."
msgstr "Heeft u al een account? Log dan <a href=\"%(login_url)s\"> in </a>."

#: templates/django_mailman3/paginator/pagination.html:45
msgid "Jump to page:"
msgstr "Ga naar pagina:"

#: templates/django_mailman3/paginator/pagination.html:64
msgid "Results per page:"
msgstr "Resultaten per pagina:"

#: templates/django_mailman3/paginator/pagination.html:80
#: templates/django_mailman3/profile/profile.html:71
msgid "Update"
msgstr "Update"

#: templates/django_mailman3/profile/base.html:6
msgid "User Profile"
msgstr "Gebruikersprofiel"

#: templates/django_mailman3/profile/base.html:13
msgid "User profile"
msgstr "Gebruikersprofiel"

#: templates/django_mailman3/profile/base.html:13
msgid "for"
msgstr "voor"

#: templates/django_mailman3/profile/base.html:24
msgid "E-mail Addresses"
msgstr "E-mailadressen"

#: templates/django_mailman3/profile/base.html:31
msgid "Account Connections"
msgstr "Accountcontacten"

#: templates/django_mailman3/profile/base.html:36
#: templates/django_mailman3/profile/delete_profile.html:16
msgid "Delete Account"
msgstr "Verwijder account"

#: templates/django_mailman3/profile/delete_profile.html:11
msgid ""
"Are you sure you want to delete your account? This will remove your account "
"along with all your subscriptions."
msgstr ""
"Weet u zeker dat u uw account wilt verwijderen? Deze handeling zal uw "
"account verwijderen, inclusief al uw abonnementen."

#: templates/django_mailman3/profile/profile.html:20
#: templates/django_mailman3/profile/profile.html:57
msgid "Edit on"
msgstr "Ga verder met bewerken"

#: templates/django_mailman3/profile/profile.html:28
msgid "Primary email:"
msgstr "Primaire e-mail:"

#: templates/django_mailman3/profile/profile.html:34
msgid "Other emails:"
msgstr "Andere e-mails:"

#: templates/django_mailman3/profile/profile.html:40
msgid "(no other email)"
msgstr "(geen verdere e-mail)"

#: templates/django_mailman3/profile/profile.html:45
msgid "Link another address"
msgstr "Koppel een ander adres"

#: templates/django_mailman3/profile/profile.html:53
msgid "Avatar:"
msgstr "Avatar:"

#: templates/django_mailman3/profile/profile.html:63
msgid "Joined on:"
msgstr "Lid geworden op:"

#: templates/django_mailman3/profile/profile.html:72
msgid "cancel"
msgstr "verwijder"

#: templates/openid/login.html:10
msgid "OpenID Sign In"
msgstr "Log in met openID"

#: templates/socialaccount/connections.html:9
msgid ""
"You can sign in to your account using any of the following third party "
"accounts:"
msgstr ""
"U kunt op uw account inloggen met gebruik van een van de volgende derde-"
"partijen-accounts:"

#: templates/socialaccount/connections.html:42
msgid ""
"You currently have no social network accounts connected to this account."
msgstr ""
"U heeft momenteel geen sociale netwerk-accounts gekoppeld aan dit account."

#: templates/socialaccount/connections.html:45
msgid "Add a 3rd Party Account"
msgstr "Voeg een derde-partij-account toe"

#: templates/socialaccount/login.html:10
#, python-format
msgid "Connect %(provider)s"
msgstr ""

#: templates/socialaccount/login.html:13
#, python-format
msgid "You are about to connect a new third-party account from %(provider)s."
msgstr ""

#: templates/socialaccount/login.html:17
#, python-format
msgid "Sign In Via %(provider)s"
msgstr ""

#: templates/socialaccount/login.html:20
#, python-format
msgid "You are about to sign in using a third-party account from %(provider)s."
msgstr ""

#: templates/socialaccount/login.html:27
msgid "Continue"
msgstr ""

#: templates/socialaccount/signup.html:11
#, python-format
msgid ""
"You are about to use your %(provider_name)s account to login to\n"
"%(site_name)s. As a final step, please complete the following form:"
msgstr ""
"U staat op het punt uw %(provider_name)s account te gebruiken\n"
"om in te loggen op %(site_name)s. Wilt u als laatste stap het volgende "
"formulier invullen:"

#: templatetags/pagination.py:43
msgid "Newer"
msgstr "Nieuwer"

#: templatetags/pagination.py:44
msgid "Older"
msgstr "Ouder"

#: templatetags/pagination.py:46
msgid "Previous"
msgstr "Vorig(e)"

#: templatetags/pagination.py:47
msgid "Next"
msgstr "Volgend(e)"

#: views/profile.py:72
msgid "The profile was successfully updated."
msgstr "Het profiel is succesvol bijgewerkt."

#: views/profile.py:74
msgid "No change detected."
msgstr "Geen wijziging gevonden."

#: views/profile.py:110
msgid "Successfully deleted account"
msgstr "Verwijdering van account geslaagd"
